const express = require('express')
const app = express()
const mongoose = require('mongoose')
require('dotenv').config()
const port = process.env.ROOT
const auth = require('./auth')
const cors = require('cors')

mongoose.connect(process.env.MONGODB, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

mongoose.connection.on('error', ()=> {
	console.log('Cannot connect to the database')
})

mongoose.connection.once('open', ()=> {
	console.log('Connected to the database')
})

const corsOptions = {
	origin: ['https://csp2frontend.vercel.app', 'http://localhost:3000', 'https://csp2frontend-onm2w336o.vercel.app', 'https://csp2frontend-git-master.piedarancel.vercel.app/', 'https://csp2frontend.piedarancel.vercel.app/'] ,
	optionsSuccessStatus: 200
}


app.use(express.json())
app.use(express.urlencoded({extended: true}))


const userRoutes = require('./routes/userRoutes')
const recordRoutes = require('./routes/recordRoutes')
const categoryRoutes = require('./routes/categoryRoutes')

app.use('/api/users', cors(corsOptions), userRoutes)
app.use('/api/records', cors(corsOptions), recordRoutes)
app.use('/api/categories', cors(corsOptions), categoryRoutes)

app.listen(port, ()=> {
	console.log(`Listening to port ${port}`)
})