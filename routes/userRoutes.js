const express = require('express')
const router = express.Router()
const UserController = require('../controllers/userController')
const auth = require('../auth')

router.get('/', (req, res) => {
	UserController.index().then(result => res.send(result))
})

router.post('/register', (req, res) => {
	UserController.register(req.body).then(result => res.send(result))
})

router.post('/login', (req, res) => {
	UserController.login(req.body).then(result => res.send(result))
})


router.get('/details', auth.checker, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.getOne(user).then(result => res.send(result))
})

router.post('/verify-google-id-token', (req, res)=> {
	UserController.verifyGoogleTokenId(req.body.tokenId).then(result => res.send(result))
})

router.patch('/:userId', auth.checker, (req, res) => {
	const args = {
		userId: req.params.userId,
		balance: req.body.balance
	}
	UserController.editBalance(args).then(result => res.send(result))
})

router.get('/:userId', auth.checker, (req, res) => {
	const arg = {
		user: auth.decode(req.headers.authorization)
	}
	UserController.getOne(arg.user).then(result => res.send(result))
})

module.exports = router