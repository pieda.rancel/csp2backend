const express = require('express')
const router = express.Router()
const auth = require('../auth')
const RecordController = require('../controllers/recordController')

router.post('/', auth.checker, (req, res) => {
	const args = {
		name: req.body.name,
		categoryId: req.body.categoryId,
		type: req.body.type,
		amount: req.body.amount,
		description: req.body.description,
		balance: req.body.balance,
		userId: auth.decode(req.headers.authorization).id
	}
	RecordController.addRecord(args).then(result => res.send(result))
})

router.delete('/:recordId', auth.checker, (req, res) => {
	console.log(auth.decode(req.headers.authorization).id)
	const args = {
		recordId: req.params.recordId,
		userId: auth.decode(req.headers.authorization).id
	}
	RecordController.toggleDelete(args).then(result => res.send(result))
})

router.put('/:recordId', auth.checker, (req, res) => {
	console.log(req.body)
	const args = {
		recordId: req.params.recordId,
		reqBody: req.body,
		user: auth.decode(req.headers.authorization)
	}
	RecordController.editRecord(args).then(result => res.send(result))
})

module.exports = router