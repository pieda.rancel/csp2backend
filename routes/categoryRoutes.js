const express = require('express')
const router = express.Router()
const CategoryController = require('../controllers/categoryController')


router.post('/', (req, res) => {
	CategoryController.addCategory(req.body).then(result => res.send(result))
})


router.get('/', (req, res) => {
	CategoryController.getCategories().then(result => res.send(result))
})


module.exports = router