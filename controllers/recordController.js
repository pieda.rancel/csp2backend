const Record = require('../models/recordModel')
const User = require('../models/userModel')

module.exports.addRecord = (params) => {
	const newRecord = new Record({
		name: params.name,
		categoryId: params.categoryId,
		type: params.type,
		amount: params.amount,
		description: params.description
	})

	return newRecord.save().then((updatedRecord, error) => {
		if(error) return false
		return User.findById(params.userId).then((user, error) => {
			if(error) return false
			user.records.push({
				_id: updatedRecord._id
			})
			return user.save().then((success, error) => {
				if(error) return false
					console.log("success", success._id)
				return success._id
			})
		})
	})
}


module.exports.toggleDelete = (params) => {
	return User.findById(params.userId).populate({path: 'records',
populate: {
	path: 'categoryId',
	model: 'Category'
}}).then((user, error) => {
		if(error) return false
		return Record.findById(params.recordId).then((record, error) => {
			console.log(record, "record")
			if(record.isActive === true){
				record.isActive = false
				if(record.type === 'Expense'){
					user.balance = user.balance + +record.amount
				} else {
					user.balance = user.balance - +record.amount
				}
			} else {
				record.isActive = true
				if(record.type === 'Expense'){
					user.balance = +user.balance - +record.amount
				} else {
					user.balance = +user.balance + +record.amount
				}
			}
			return record.save().then((updatedRecord, error) => {
				console.log(updatedRecord, "updatedRecord")
				if(error) return false
					return user.save().then((user, error) => {
						if(error) return false
						console.log(user.balance, "User")
						return user
					})
			})
		})
	})
}


module.exports.editRecord = (params) => {
	return Record.findByIdAndUpdate(params.recordId, params.reqBody, {new: true}).then((record, error) => {
		if (error) return false
		return User.findById(params.user.id).populate({ 
			path: 'records',
			populate: {
				path: 'categoryId',
				model: 'Category'
			}}).then(user => {
			return user
		})
	})
}