const Category = require('../models/categoryModel')

module.exports.addCategory = (params) => {
	const category = new Category({
		name: params.name
	})

	return category.save().then(updatedCategory => {
		return Category.find().then(categories => {
			return categories
		})
	})
}

module.exports.getCategories = (params) => {
	return Category.find().then((categories, error) => {
		if(error) return false
		return categories
	})
}

