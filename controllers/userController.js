const User = require('../models/userModel')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {OAuth2Client} = require('google-auth-library')
const clientId = "91423877803-qut6en98op7ds4i7jr30e9o104rvbi5m.apps.googleusercontent.com"


module.exports.index = (params) => {
	return User.find({}).then((resolve, reject) => {
		return reject ? false : resolve
	})
}

module.exports.register = (params) => {
	return User.findOne({email: params.email}).then(email => {
		if(email !== null){
			return false
		} else {
			const user = new User({
				firstName: params.firstName,
				lastName: params.lastName,
				email: params.email,
				password: bcrypt.hashSync(params.password, 10),
				isAdmin: params.isAdmin,
			})

			return user.save().then((resolved, rejected) => {
				return rejected ? false : true
			})
	}
	})
}

module.exports.login = (params) => {
		return User.findOne({email: params.email}).then(resolvedData => {
			if(resolvedData === null){
				return false
			} else {
				const isMatched = bcrypt.compareSync(params.password, resolvedData.password)
				if(isMatched){
					return {
						accessToken: auth.createAccessToken(resolvedData)
					}
				} else {
					return false
				}
			}
		})
}

module.exports.getOne = (params) => {
	return User.findById(params.id).populate({
		path: 'records',
		populate: {
			path: 'categoryId',
			model: 'Category',
		}
	}).then(resolvedData => {
		resolvedData.password = undefined
		return resolvedData
	})
}


module.exports.verifyGoogleTokenId = async (tokenId) => {
const client = new OAuth2Client(clientId)
const data = await client.verifyIdToken({idToken: tokenId, audience: clientId})
/*
If the verification process is successful, the google account detail shall contain by the data variable, and data variable has a property called 'payload', which contains the information about the user
*/
if(data.payload.email_verified){
    const user = await User.findOne({email: data.payload.email}).exec()
    if(user !== null){
        // console.log(user)
        return user.loginType ==='google' ? {accessToken: auth.createAccessToken(user.toObject())} : {error: 'login-type-error'}
    } else {
    const newUser = new User({
        firstName: data.payload.given_name,
        lastName: data.payload.family_name,
        email: data.payload.email,
        loginType: 'google'
    })

        return newUser.save().then((user, error) => {
            return error ? null : auth.createAccessToken(user.toObject())
                })
        }
    } else {
        return {error: 'google-auth-error'}
    }
} 


module.exports.editBalance = (params) => {
	return User.findByIdAndUpdate(params.userId, {
		balance: params.balance
	}, {new:true}).populate({
		path: 'records',
		populate: {
			path: 'categoryId',
			model: 'Category'
		}
	}).then((user, error) => {
		if(error) return false
			console.log(user)
		return user
	})
}