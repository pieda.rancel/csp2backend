const mongoose = require('mongoose')
const Schema = mongoose.Schema

const categorySchema = new Schema({
	name: {
		type: String,
		required: [true, "Category Name is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	}
})

module.exports = mongoose.model('Category', categorySchema)

