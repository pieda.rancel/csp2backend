const mongoose = require('mongoose')
const Schema = mongoose.Schema

const recordSchema = new Schema({
	name: {
		type: String,
		required: [true, "Record Name is required"]
	},
	categoryId: {
		type: Schema.Types.ObjectId,
		ref: 'Category'
	},
	type: {
		type: String,
		required: [true, "Budget Type is required"]
	},
	amount: {
		type: Number,
		required: [true, "Amount is required"]
	},
	description: {
		type: String
	},
	dateAdded: {
		type: Date,
		default: Date.now
	},
	isActive: {
		type: Boolean,
		default: true
	}
})


module.exports = mongoose.model('Record', recordSchema)