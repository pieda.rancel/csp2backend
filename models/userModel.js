const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required']
	},
	email: {
		type: String,
		required: [true, 'E-mail is required']
	},
	password: {
		type: String,
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	balance: {
		type: Number,
		default: 0
	},
	loginType: {
		type: String,
		default: 'manual'
	},
	records: [
	{
		type: Schema.Types.ObjectId,
		ref: 'Record'
	}]
})


module.exports = mongoose.model('User', userSchema)